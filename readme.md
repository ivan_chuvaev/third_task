grade by faculty:  
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=avg&faculty=Faculty%20of%20security  
grade can be: min, max, avg

grade by sex:  
&ensp;&ensp;http://127.0.0.1:3000/get_grade?grade=avg&sex=male   
sex can be: male, female  

sort students by grade:  
http://127.0.0.1:3000/students?sort=gradeDown&faculty=Faculty%20of%20law  
sort can be: gradeDown, gradeUp

student info by id:  
http://127.0.0.1:3000/student/{id} 

edit student with post request:  
http://127.0.0.1:3000/student/{id}   
post's body can contain: firstName, lastNmae, phone, sex, faculty, averageGrade

students by faculty and sex:
http://127.0.0.1:3000/students?faculty=Faculty%20of%20security&sort=gradeUp&sex=male

