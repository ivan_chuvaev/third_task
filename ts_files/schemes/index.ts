'use strict';
import * as Joi from 'joi';
import { Faculty } from '../models';
import { sex, maxGrade } from '../models/student_model';

export const studentInfoSchema = Joi.object({

    id: Joi.string().guid()

})

export const studentEditSchema = Joi.object({

    firstName: Joi.string(),
    lastName: Joi.string(),
    sex: Joi.string().valid(...sex),
    faculty: Joi.any().external(facultyValidationMethod),
    phone: Joi.string().pattern(/^[0-9]+$/),
    averageGrade: Joi.number().positive().max(maxGrade)

})

export const getGradeShema = Joi.alternatives().try(

    Joi.object({
        sex: Joi.string().valid(...sex).required(),
        grade: Joi.string().valid('avg', 'max', 'min').required()
    }),

    Joi.object({
        faculty: Joi.any().external(facultyValidationMethod).required(),
        grade: Joi.string().valid('avg', 'max', 'min').required()
    })

)

export const listStudentsSchema = Joi.object({

    faculty: Joi.any().external(facultyValidationMethod).required(),
    sex: Joi.string().valid(...sex),
    sort: Joi.string().valid('gradeUp', 'gradeDown'),
    resultsOnPage: Joi.number().valid(5,10,20,50,100),
    page: Joi.number().integer().min(0)

})


async function facultyValidationMethod(value:any){
    if (!await Faculty.findOne({
            where:{
                name: value
            }
        })
    )
        throw Error("any.invalid");

    return value;

}