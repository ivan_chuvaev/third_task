'use strict';
import * as handlers from '../handlers';
import * as schemas from '../schemes';

export const routes = [

    //this route will return only one number
    {
        method: 'GET',
        path: '/get_grade',
        options:{
            validate:{
                query: schemas.getGradeShema
            }
        },
        handler: handlers.getGradeHandler
    },

    //faculty's students with grade ordering, now sex can be provided
    {
        method: 'GET',
        path: '/students',
        options:{
            validate:{
                query: schemas.listStudentsSchema
            }
        },
        handler: handlers.listStudentsHandler
    },

    //student info page
    {
        method: 'GET',
        path: '/student/{id}',
        options: {
            validate: {
                params: schemas.studentInfoSchema
            }
        },
        handler: handlers.studentInfoHandler
    },

    //edit student info
    {
        method: 'POST',
        path: '/student/{id}',
        options: {
            validate: {
                params: schemas.studentInfoSchema,
                payload: schemas.studentEditSchema
            }
        },
        handler: handlers.studentEditHandler
    }
]