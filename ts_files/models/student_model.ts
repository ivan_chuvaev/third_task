'use strict';

import { Column, ForeignKey, DataType, Table, Model, PrimaryKey, BelongsTo } from 'sequelize-typescript';
import { Faculty } from './faculty_model';


export const sex:string[]=['male', 'female'];
enum sexEnum{
    'male',
    'female'
}

export const maxGrade:number = 5;

export const phoneLength:number = 11;

@Table
export class Student extends Model {
    @PrimaryKey
    @Column({
        type: DataType.UUID,
        allowNull: false,
        defaultValue: DataType.UUIDV4
    }) id:string;

    @Column({
        type: DataType.STRING,
        allowNull: true
    }) firstName:string;

    @Column({
        type: DataType.STRING,
        allowNull: true
    }) lastName:string;

    @Column({
        type: DataType.ENUM({values: sex}),
        allowNull: true
    }) sex:string;

    @Column({
        type: DataType.STRING,
        allowNull: true
    }) phone:string;

    @Column({
        type: DataType.FLOAT,
        allowNull: true
    }) averageGrade:number;

    @ForeignKey(()=>Faculty)
    @Column({
        type: DataType.INTEGER
    })
    facultyID:number;

    @BelongsTo(() => Faculty)
    facultyInstance: Faculty
}