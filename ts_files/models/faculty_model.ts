'use strict';

import { Column, Table, Model, DataType, PrimaryKey, AutoIncrement, HasMany, Default } from 'sequelize-typescript';
import { Student } from './student_model';

@Table
export class Faculty extends Model {
    
    @PrimaryKey
    @AutoIncrement
    @Column ({
        type: DataType.INTEGER,
        allowNull: false
    }) id:number;

    @Column ({

        type: DataType.STRING,
        allowNull: false

    }) name:string

    @HasMany(() => Student)
    players: Student[];
}