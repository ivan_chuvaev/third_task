import { fillFacultyTable } from './faculty_model_filling';
import { fillStudendsTable } from './students_model_filling';

export { fillFacultyTable, fillStudendsTable };