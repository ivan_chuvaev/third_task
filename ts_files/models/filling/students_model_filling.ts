'use strict';

import { randInt, randNumberString } from '../../utils';
import { Student, maxGrade, phoneLength, sex } from '../student_model';
import { Faculty } from '../faculty_model';
import { Sequelize } from 'sequelize-typescript';

const studentsCount = 100;

const firstNames:string[] = [
    'Liam','Noah','Oliver','Elijah','William','James','Benjamin','Lucas','Henry','Alexander','Mason','Michael',
    'Ethan','Daniel','Jacob','Logan','Jackson','Levi','Sebastian','Mateo','Jack','Owen','Theodore','Aiden',
    'Samuel','Joseph','John','David','Wyatt','Matthew','Luke','Asher','Carter','Julian','Grayson','Leo',
    'Jayden','Gabriel','Isaac','Lincoln','Anthony','Hudson','Dylan','Ezra','Thomas','Charles','Christopher',
    'Jaxon','Maverick','Josiah','Isaiah','Andrew','Elias','Joshua','Nathan','Caleb','Ryan'
]
const lastNames:string[] = [
    'Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Garcia', 'Miller', 'Davis', 'Rodriguez', 'Martinez',
    'Hernandez', 'Lopez', 'Gonzalez', 'Wilson', 'Anderson', 'Thomas', 'Taylor', 'Moore', 'Jackson', 'Martin',
    'Lee', 'Perez', 'Thompson', 'White', 'Harris', 'Sanchez', 'Clark', 'Ramirez', 'Lewis', 'Robinson', 'Walker',
    'Young', 'Allen', 'King', 'Wright', 'Scott', 'Torres', 'Nguyen', 'Hill', 'Flores', 'Green', 'Adams', 'Nelson',
    'Baker', 'Hall', 'Rivera', 'Campbell', 'Mitchell', 'Carter', 'Roberts'
]

export async function fillStudendsTable(){

    let studentsArray:Promise<Student>[] = new Array(studentsCount);

    for(let i = 0; i < studentsCount; i++){
        studentsArray[i] = Student.create({

            firstName: firstNames[randInt(firstNames.length-1)],
            lastName: lastNames[randInt(lastNames.length-1)],
            sex: sex[randInt(sex.length-1)],
            phone: randNumberString(phoneLength),
            facultyID: (await Faculty.findOne({order:Sequelize.literal('random()')})).id,
            averageGrade: Math.random()*maxGrade 

        })
    }

    await Promise.all(studentsArray);
};