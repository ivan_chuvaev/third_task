import { Faculty } from "../faculty_model";

const faculties_arr:string[] = [
    'Faculty of radio engineering', 'Faculty of radio design',
    'Faculty of computer systems', 'Faculty of control systems',
    'Faculty of electronic engineering', 'Faculty of economics',
    'Faculty of innovation technologies', 'Faculty of human sciences',
    'Faculty of law', 'Faculty of security',
    'Faculty of distance learning', 'Faculty of extramural and evening education',
    'Faculty of advanced training'
];

export async function fillFacultyTable(){

    let faculty_entries:Promise<Faculty>[] = new Array(faculties_arr.length);

    for(let i = 0; i < faculty_entries.length; i++){
        faculty_entries[i] = Faculty.create({

            name: faculties_arr[i]

        })
    }

    await Promise.all(faculty_entries);
};