import { Student } from "./student_model";
import { Faculty } from "./faculty_model";
import { Sequelize } from 'sequelize-typescript';

export const sequelize = new Sequelize('postgres://postgres:123321@localhost:5432/crypton_bd', {
    dialect: 'postgres',
    models: [Student, Faculty],
    logging: false
});

export { Student, Faculty };