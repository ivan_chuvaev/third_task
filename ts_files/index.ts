'use strict';

import { Server } from "@hapi/hapi";
import { routes } from "./routes";
import { bd_connection_test } from "./utils";
import { Student, Faculty, sequelize } from "./models";
import { fillStudendsTable, fillFacultyTable } from "./models/filling";

const init = async () => {

    const server = new Server({
        port: 3000,
        host: 'localhost'
    });

    server.route(routes);

    await server.start();
    console.log('Server running on %s', server.info.uri);
    
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);

});

(async ()=>{

    if(await bd_connection_test(sequelize)){

        await sequelize.sync();

        if(await Faculty.count() == 0)
            await fillFacultyTable();

        if(await Student.count() == 0)
            await fillStudendsTable();

        init();
    }
})();