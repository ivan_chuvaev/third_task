import { Sequelize } from 'sequelize-typescript';

export function randNumberString(length:number):string{
    let ret:string = '';
    for(let i = 0; i < length; i++){
        ret += Math.floor(Math.random()*10).toString();
    }
    return ret
}

export function randInt(max:number):number{
    return Math.floor(Math.random()*(max+1));
}

export function getObjectWithExactProperties(inputOjb:any, props:string[]){

    let arr:object[] = new Array<object>(props.length);
    props.forEach((element:string, index:number)=>{
        arr[index] = inputOjb[element] ? { [element]: inputOjb[element] } : undefined;
    });

    return Object.assign({}, ...arr)
    
}

export async function bd_connection_test(sequelize: Sequelize):Promise<boolean>{
    try {

        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        return true;

      } catch (error) {

        console.error('Unable to connect to the database:', error);
        return false;

      }
}