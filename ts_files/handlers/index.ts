'use strict';

import {Request, ResponseToolkit} from "@hapi/hapi";
import { Sequelize } from 'sequelize-typescript';
import { Faculty, sequelize } from "../models";
import {Student} from "../models/student_model";
import {getObjectWithExactProperties} from "../utils";

export async function getGradeHandler (request: Request, reply: ResponseToolkit) {

    let options:any = {
        attributes:[
            [
                Sequelize.fn(
                  request.query.grade,
                  Sequelize.col('averageGrade'),
                ),
                request.query.grade
            ],
        ],
        where: getObjectWithExactProperties(request.query, ['sex']),
        include: [{
            model: Faculty,
            where:{
                name: request.query.faculty
            }
        }],
        group : ['facultyInstance.id', 'facultyInstance.name', 'facultyInstance.createdAt','facultyInstance.updatedAt'],
    }

    if (!request.query.faculty){
        delete options.include;
        delete options.group;
    }

    return (await Student.findAll(options) as any)[0].dataValues[request.query.grade];

}

export async function listStudentsHandler(request: Request, reply: ResponseToolkit){

    let limit = request.query.resultsOnPage ? request.query.resultsOnPage : 50
    
    let students:Student[] = await Student.findAll({
        where: getObjectWithExactProperties(request.query, ['sex']),
        include: [{
            model: Faculty,
            where:{
                name: request.query.faculty
            }
        }],
        order: [['averageGrade', (request.query.sort == 'gradeDown') ? 'DESC' : 'ASC']],
        limit: limit,
        offset: request.query.page ? request.query.page * limit : 0
    });

    if(!students)
        return reply.response('database is empty').code(500);

    return students;

}

export async function studentInfoHandler(request: Request, reply: ResponseToolkit) {
            
    const student = await Student.findOne({
        where: {id:request.params.id}
    })

    if(!student)
        return reply.response(`student ${request.params.id} not found`).code(404);

    return student;

}

export async function studentEditHandler(request: Request, reply: ResponseToolkit){
    
    const student = await Student.findOne({
        where: {id:request.params.id}
    })

    if(!student)
        return reply.response(`student ${request.params.id} not found`).code(404);
    
    let payload:any = request.payload;
    let mstudent:any = student;
    
    Object.keys(mstudent.dataValues).forEach((key:string)=>{

        if(payload[key])
            mstudent[key] = payload[key]

    })
    await mstudent.save();

    return mstudent;
}